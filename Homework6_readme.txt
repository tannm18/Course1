This program print the sum of two number using modified sum function.
If this sum lies within the range from 15 to 20 then the sum function will return 20 else return 15.
1. sum(10, 6) = 16 ==> return 20
2. sum(10, 2) = 12 ==> return 15
3. sum(10, 20) = 30 ==> return 15